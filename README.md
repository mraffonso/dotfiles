dotfiles
========

A collection of config files that I use in my day to day escapades.

Git
---

Installs .gitconfig file.

**Action required**: Set email address.

Mercurial
--

Installs .hgrc file.

**Action required**: Set email address.

NeoVim
------

Symlinks .vimrc file.

Rails
-----

Symlinks .railsrc file.

Ruby gems
---------

Symlinks .gemrc file.

Tmux
----

Symlinks .tmux.conf file.

Vim
---

Symlinks .vimrc file.
Symlinks .vim directory.
